# GTP Proxy

The GTP proxy is deployed as helm chart. The package is available in
charts.tplabs.net (see 1password for credentials).

## Install

Add the helm repo if not present yet:
```
helm repo add tplabs https://chart:<pw>@charts.tplabs.net/
```

Install the chart with given config and name for a dedicated zone:
```
helm install -n gtp-a -f values-gtp-proxy-a.yaml tplabs/gtp-proxy
```

## Upgrade

```
helm upgrade gtp-a -f values-gtp-proxy-a.yaml tplabs/gtp-proxy
helm upgrade gtp-b -f values-gtp-proxy-b.yaml tplabs/gtp-proxy
```


## Operation

### Ping

Ping to EMnify endpoints:
```
ping -I core0 10.70.3.15
```

Ping to GRX endpoints via TDG:
```
ping -I acc0 213.162.79.230
```

The GTP Proxy uses VRF-lite in order to have separate routing tables and
default routes in the access and core interfaces.

### Trigger GTP echo

Send echo on v1 and v2 to Emnify in zone A
```
gtp_socket_reg:all().
GtpPortEm = gtp_socket_reg:lookup('irx-1').
{ok,PidV1Em} = gtp_path_sup:new_path(GtpPortEm, v1, {10,70,3,15}, []).
gen_server:call(PidV1Em, {bind,self()}).
{ok,PidV2Em} = gtp_path_sup:new_path(GtpPortEm, v2, {10,70,3,15}, []).
gen_server:call(PidV2Em, {bind,self()}).
```

Send echo on v1 and v2 to Emnify in zone B
```
{ok,PidV1Em} = gtp_path_sup:new_path(GtpPortEm, v1, {10,70,4,15}, []).
gen_server:call(PidV1Em, {bind,self()}).
{ok,PidV2Em} = gtp_path_sup:new_path(GtpPortEm, v2, {10,70,4,15}, []).
gen_server:call(PidV2Em, {bind,self()}).
```

TDG Echo to TDG
```
GtpPortTdg = gtp_socket_reg:lookup('irx-0').
{ok,PidV1Tdg} = gtp_path_sup:new_path(GtpPortTdg, v1, {193,254,138,12}, []).
gen_server:call(PidV1Tdg, {bind,self()}).
GtpPortTdg = gtp_socket_reg:lookup('irx-0').
{ok,PidV2Tdg} = gtp_path_sup:new_path(GtpPortTdg, v2, {193,254,138,12}, []).
gen_server:call(PidV2Tdg, {bind,self()}).
```

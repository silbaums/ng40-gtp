# Helm Chart Version

This file states the helm chart version and subsequentially the application
version that is expected to run in the cluster. It also keeps a history of what
was changed. Details can be found in the respective CHANGELOG.md of the
product repository, e.g. for [GTP Rroxy
Controller](https://gitlab.tpip.net/travelping/products/gtp-proxy/blob/master/CHANGELOG.md).


## 1.2.0

- Metrics are exposed via prometheus.
- Annotation triggers automatic scraping

## 1.1.0

- Support for multiple PGWs
- *Config change required*

